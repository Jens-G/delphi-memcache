object frmMemCacheTest: TfrmMemCacheTest
  Left = 0
  Top = 0
  Caption = 'MemCache Test'
  ClientHeight = 328
  ClientWidth = 268
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 268
    Height = 328
    ActivePage = tsFunctions
    Align = alClient
    TabOrder = 0
    object tsServers: TTabSheet
      Caption = 'Servers'
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        260
        300)
      object Label3: TLabel
        Left = 8
        Top = 8
        Width = 163
        Height = 13
        Caption = 'Servers (default 100% localhost):'
      end
      object txtServers: TMemo
        Left = 8
        Top = 27
        Width = 241
        Height = 226
        Anchors = [akLeft, akTop, akRight, akBottom]
        Lines.Strings = (
          '100=127.0.0.1:11211')
        TabOrder = 0
      end
      object bnRestart: TButton
        Left = 174
        Top = 261
        Width = 75
        Height = 29
        Anchors = [akRight, akBottom]
        Caption = 'Restart'
        TabOrder = 1
        OnClick = bnRestartClick
      end
    end
    object tsFunctions: TTabSheet
      Caption = 'Functions'
      ImageIndex = 1
      Constraints.MinHeight = 300
      Constraints.MinWidth = 260
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      DesignSize = (
        260
        300)
      object Label1: TLabel
        Left = 8
        Top = 8
        Width = 22
        Height = 13
        Caption = 'Key:'
      end
      object Label2: TLabel
        Left = 8
        Top = 56
        Width = 30
        Height = 13
        Caption = 'Value:'
      end
      object btnStore: TButton
        Left = 8
        Top = 202
        Width = 75
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Store'
        TabOrder = 0
        OnClick = btnStoreClick
      end
      object edtKey: TEdit
        Left = 8
        Top = 27
        Width = 237
        Height = 21
        Anchors = [akLeft, akTop, akRight]
        TabOrder = 1
        Text = 'testkey'
      end
      object btnLookup: TButton
        Left = 8
        Top = 233
        Width = 75
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Lookup'
        TabOrder = 2
        OnClick = btnLookupClick
      end
      object btnDelete: TButton
        Left = 170
        Top = 264
        Width = 75
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Delete'
        TabOrder = 3
        OnClick = btnDeleteClick
      end
      object btnIncrement: TButton
        Left = 170
        Top = 202
        Width = 75
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Increment'
        TabOrder = 4
        OnClick = btnIncrementClick
      end
      object btnDecrement: TButton
        Left = 170
        Top = 233
        Width = 75
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Decrement'
        TabOrder = 5
        OnClick = btnDecrementClick
      end
      object btnAppend: TButton
        Left = 89
        Top = 202
        Width = 75
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Append'
        TabOrder = 6
        OnClick = btnAppendClick
      end
      object btnPrepend: TButton
        Left = 89
        Top = 233
        Width = 75
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Prepend'
        TabOrder = 7
        OnClick = btnPrependClick
      end
      object btnInsert: TButton
        Left = 8
        Top = 264
        Width = 75
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Insert'
        TabOrder = 8
        OnClick = btnInsertClick
      end
      object btnReplace: TButton
        Left = 89
        Top = 264
        Width = 75
        Height = 25
        Anchors = [akLeft, akBottom]
        Caption = 'Replace'
        TabOrder = 9
        OnClick = btnReplaceClick
      end
      object edtValue: TMemo
        Left = 8
        Top = 75
        Width = 237
        Height = 121
        Anchors = [akLeft, akTop, akRight, akBottom]
        Lines.Strings = (
          '1')
        ScrollBars = ssBoth
        TabOrder = 10
        WordWrap = False
      end
    end
  end
end
