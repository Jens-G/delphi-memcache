unit uMainForm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  IdGlobal, EncdDecd, Math, CommonTestTools,
  Dialogs, StdCtrls, MemCache, ComCtrls;


type
  TfrmMemCacheTest = class(TForm)
    PageControl1: TPageControl;
    tsServers: TTabSheet;
    tsFunctions: TTabSheet;
    btnStore: TButton;
    Label1: TLabel;
    edtKey: TEdit;
    Label2: TLabel;
    btnLookup: TButton;
    btnDelete: TButton;
    btnIncrement: TButton;
    btnDecrement: TButton;
    btnAppend: TButton;
    btnPrepend: TButton;
    btnInsert: TButton;
    btnReplace: TButton;
    Label3: TLabel;
    txtServers: TMemo;
    bnRestart: TButton;
    edtValue: TMemo;
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure bnRestartClick(Sender: TObject);
    procedure btnStoreClick(Sender: TObject);
    procedure btnLookupClick(Sender: TObject);
    procedure btnInsertClick(Sender: TObject);
    procedure btnAppendClick(Sender: TObject);
    procedure btnPrependClick(Sender: TObject);
    procedure btnReplaceClick(Sender: TObject);
    procedure btnIncrementClick(Sender: TObject);
    procedure btnDecrementClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
  public
    { Public declarations }
  end;

var
  frmMemCacheTest: TfrmMemCacheTest;
  MemCache : TMemCache;

implementation

{$R *.dfm}

procedure TfrmMemCacheTest.FormCreate(Sender: TObject);
begin
  PageControl1.ActivePageIndex := 0;
  MemCache := TMemCache.Create;
end;

procedure TfrmMemCacheTest.FormDestroy(Sender: TObject);
begin
  MemCache.Free;
end;


procedure TfrmMemCacheTest.btnAppendClick(Sender: TObject);
begin
  MemCache.Append( AnsiString(edtKey.Text), edtValue.Text);
  btnLookupClick(Sender);
end;

procedure TfrmMemCacheTest.btnDecrementClick(Sender: TObject);
begin
  edtValue.Text := IntToStr(MemCache.Decrement( AnsiString(edtKey.Text)));
  edtValue.SetFocus;
end;

procedure TfrmMemCacheTest.btnDeleteClick(Sender: TObject);
begin
  MemCache.Delete( AnsiString(edtKey.Text));
  edtKey.SetFocus;
end;

procedure TfrmMemCacheTest.btnIncrementClick(Sender: TObject);
begin
  edtValue.Text := IntToStr(MemCache.Increment( AnsiString(edtKey.Text)));
  edtValue.SetFocus;
end;

procedure TfrmMemCacheTest.btnInsertClick(Sender: TObject);
begin
  try
    MemCache.Insert( AnsiString(edtKey.Text), edtValue.Text);
  finally
    btnLookupClick(Sender);
  end;
end;

procedure TfrmMemCacheTest.btnLookupClick(Sender: TObject);
begin
  edtValue.Text := string( MemCache.Lookup( AnsiString( edtKey.Text)).Value);
  edtValue.SetFocus;
end;

procedure TfrmMemCacheTest.btnPrependClick(Sender: TObject);
begin
  MemCache.Prepend( AnsiString(edtKey.Text), edtValue.Text);
  btnLookupClick(Sender);
end;

procedure TfrmMemCacheTest.btnReplaceClick(Sender: TObject);
begin
  try
    MemCache.Replace( AnsiString(edtKey.Text), edtValue.Text);
  finally
    btnLookupClick(Sender);
  end;
end;

procedure TfrmMemCacheTest.btnStoreClick(Sender: TObject);
begin
  MemCache.Store( AnsiString(edtKey.Text), edtValue.Text);
  edtValue.SetFocus;
end;

procedure TfrmMemCacheTest.bnRestartClick(Sender: TObject);
begin
  FreeAndNil(MemCache);
  MemCache := TMemCache.Create(txtServers.Lines);
end;






end.
